# Ansible Playbook debuging

**Run nur eine TASKS**

`ansible-playbook  -i ~/.ansible/hosts.cfg ~/.ansible/playbooks/rp3.yml --ask-pass  --start-at-task="Update apt cache."`


**Try Run**

`ansible-playbook --ask-become-pass --diff --check -i --become-user --user $USER --private-key --tags setup-install`

**syntax-check**

`ansible-playbook --ask-become-pass --diff --syntax-check -i --become-user --user $USER --private-key --tags setup-install`