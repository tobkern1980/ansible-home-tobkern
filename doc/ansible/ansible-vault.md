# Ansible vault

Sensible Daten Managen

## Ansible Vault in Playbooks einsetzen

**Neue Verschlüsselte Datei anlegen**

`ansible-vault create stack.yml`

**Datein entschlüßeln**

`ansible-vault encrypt stack.yml`

**Neue Verschlüsselte Datei ansehen**

`ansible-vault view stack.yml`

**Neue Verschlüsselte Datei anpassen**

`ansible-vault edit stack.yml`

**Password ändern**

`ansible-vault rekey stack.yml`

**Benutzung mit copy**

`ansible --ask-vault-pass -bK -m copy -a 'src=stag dest=/tmp/stag mode=0600 owner=root group=root' localhost`

**ansible Playbook book mit Vault password file benutzen**

`ansible-playbook site.yml --vault-password-file ~/.vault_pass.txt`

**via Script**

`ansible-playbook site.yml --vault-password-file ~/.vault_pass.py`

Alternativ kann auch via shell Variablen übergeben 
[Ansible Doc hier ist _Environment_ der wichtige Teil.](https://docs.ansible.com/ansible/latest/reference_appendices/config.html#default-vault-id-match) 
und können dan in einer User Umgebung hinterlegt werden.

`~/.ansible-env`

**Lesen der Password Datei automatisch**

`export ANSIBLE_VAULT_PASSWORD_FILE=~/.ansible-env`

Script das die Umgebung setzt.

```py
#!/usr/bin/env python

import os
print os.environ['VAULT_PASSWORD']
```

Das skript muss auch ausführbar sein `chmod +x .vault_pass`.

Weiteres [how-to-use-vault-to-protect-sensitive-ansible-data](https://www.digitalocean.com/community/tutorials/how-to-use-vault-to-protect-sensitive-ansible-data-on-ubuntu-16-04)

[ansible-vault-example-encrypt-string-playbook](https://www.golinuxcloud.com/ansible-vault-example-encrypt-string-playbook/)


Sollte das PAssword aus einem Password manager übergeben.

Mehrere Passwörter mit ansible Vault benutzen

`ansible-playbook site.yml --vault-id dev@~/.vault_pass.txt --vault-id prod@prompt`

Für die Zuordung der Umgebung wird hierfür dann die [Labelling Vault](https://docs.ansible.com/ansible/latest/user_guide/vault.html#labelling-vaults)

Als Beispiel

- **staging** ist die Umgebung vor der Production benutzung zum testen
- **production** ist die Productive umgebung
- **development** Für tests inerhalb der Entwicklung für die Entwickeler
- **testing** Für systemtestes

- **staging-password** Staging vault Password file
- **production-password** Production vault Password file
- **development-password** Development vault Password file
- **testing-password** Testing vault Password file

**Password via textfile**

`ansible-playbook --vault-id staging-password playbook.yml`

**Password via textfile und der ID umgebung _stag_**

`ansible-playbook --vault-id stag@staging-password playbook.yml`

**Password via Prompt**

`ansible-playbook --vault-id @prompt playbook.yml`

**Password via Prompt und der ID umgebung _stag_**

`ansible-playbook --vault-id stag@prompt playbook.yml`

**Password via Ausführbarem Script**

`ansible-playbook --vault-id my-vault-password.py`

**Password via Ausführbarem Script und der ID umgebung _stag_**

`ansible-playbook --vault-id stag@my-vault-password.py`

_Environment Var_ = DEFAULT_VAULT_ID_MATCH

> Info: Für die Vault ID wird Ansible >=2.4 benötigt!

> Info: Zum beschleunigen ver Vault Operations sollte `pip install cryptography` Installiert sein.

[Ansible Client Script](https://raw.githubusercontent.com/alibaba/ansible-provider-docs/master/contrib/vault/vault-keyring-client.py)

```python
#!/usr/bin/env python
# -*- coding: utf-8 -*-
# (c) 2014, Matt Martz <matt@sivel.net>
# (c) 2016, Justin Mayer <https://justinmayer.com/>
# This file is part of Ansible.
#
# Ansible is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Ansible is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Ansible.  If not, see <http://www.gnu.org/licenses/>.
#
# =============================================================================
#
# This script is to be used with ansible-vault's --vault-id arg
# to retrieve the vault password via your OS's native keyring application.
#
# This file *MUST* be saved with executable permissions. Otherwise, Ansible
# will try to parse as a password file and display: "ERROR! Decryption failed"
#
# The `keyring` Python module is required: https://pypi.org/project/keyring/
#
# By default, this script will store the specified password in the keyring of
# the user that invokes the script. To specify a user keyring, add a [vault]
# section to your ansible.cfg file with a 'username' option. Example:
#
# [vault]
# username = 'ansible-vault'
#
# In useage like:
#
#    ansible-vault --vault-id keyring_id@contrib/vault/vault-keyring-client.py view some_encrypted_file
#
#  --vault-id will call this script like:
#
#     contrib/vault/vault-keyring-client.py --vault-id keyring_id
#
# That will retrieve the password from users keyring for the
# keyring service 'keyring_id'. The equilivent of:
#
#      keyring get keyring_id $USER
#
# If no vault-id name is specified to ansible command line, the vault-keyring-client.py
# script will be called without a '--vault-id' and will default to the keyring service 'ansible'
# This is equilivent to:
#
#    keyring get ansible $USER
#
# You can configure the `vault_password_file` option in ansible.cfg:
#
# [defaults]
# ...
# vault_password_file = /path/to/vault-keyring-client.py
# ...
#
# To set your password, `cd` to your project directory and run:
#
#   # will use default keyring service / vault-id of 'ansible'
#   /path/to/vault-keyring-client.py --set
#
# or to specify the keyring service / vault-id of 'my_ansible_secret':
#
#  /path/to/vault-keyring-client.py --vault-id my_ansible_secret --set
#
# If you choose not to configure the path to `vault_password_file` in
# ansible.cfg, your `ansible-playbook` command might look like:
#
# ansible-playbook --vault-id=keyring_id@/path/to/vault-keyring-client.py site.yml

ANSIBLE_METADATA = {'status': ['preview'],
                    'supported_by': 'community',
                    'version': '1.0'}

import argparse
import sys
import getpass
import keyring

from ansible.config.manager import ConfigManager

KEYNAME_UNKNOWN_RC = 2


def build_arg_parser():
    parser = argparse.ArgumentParser(description='Get a vault password from user keyring')

    parser.add_argument('--vault-id', action='store', default=None,
                        dest='vault_id',
                        help='name of the vault secret to get from keyring')
    parser.add_argument('--username', action='store', default=None,
                        help='the username whose keyring is queried')
    parser.add_argument('--set', action='store_true', default=False,
                        dest='set_password',
                        help='set the password instead of getting it')
    return parser


def main():
    config_manager = ConfigManager()
    username = config_manager.data.get_setting('vault.username')
    if not username:
        username = getpass.getuser()

    keyname = config_manager.data.get_setting('vault.keyname')
    if not keyname:
        keyname = 'ansible'

    arg_parser = build_arg_parser()
    args = arg_parser.parse_args()

    username = args.username or username
    keyname = args.vault_id or keyname

    # print('username: %s keyname: %s' % (username, keyname))

    if args.set_password:
        intro = 'Storing password in "{}" user keyring using key name: {}\n'
        sys.stdout.write(intro.format(username, keyname))
        password = getpass.getpass()
        confirm = getpass.getpass('Confirm password: ')
        if password == confirm:
            keyring.set_password(keyname, username, password)
        else:
            sys.stderr.write('Passwords do not match\n')
            sys.exit(1)
    else:
        secret = keyring.get_password(keyname, username)
        if secret is None:
            sys.stderr.write('vault-keyring-client could not find key="%s" for user="%s" via backend="%s"\n' %
                             (keyname, username, keyring.get_keyring().name))
            sys.exit(KEYNAME_UNKNOWN_RC)

        # print('secret: %s' % secret)
        sys.stdout.write('%s\n' % secret)

    sys.exit(0)


if __name__ == '__main__':
    main()
```

**Example**

`contrib/vault/vault-keyring-client.py --vault-id dev`

Hier wird _stag_ übergeben an das script, dass so das Password für die umgebung übergibt.

Alleine mit dem Script kann man in der Praxis noch nicht viel machen.
Um es etwas Pracktischer zu machen solle man sich weitere scripte anlegen die von einem zentralem script aus aufgerufen werden

**Mögliche PAssword Manger**

* [onepassword](https://www.enpass.io/)
* [KeePassXC](https://keepassxc.org/download/#windows)
* [Bitwarden](https://bitwarden.com/)
* [Buttercup](https://buttercup.pw/)
* [Pass](https://www.passwordstore.org/)
* [Docker vault](https://hub.docker.com/_/vault)
* [Sicherheit](/sicherheit)

### Ansible Vault Variablen einsetzen

### Ansible Vault Format

Eine vault Datei ist ein UTF-8 encodierte test Datei.

Mit foldendem Header

`$ANSIBLE_VAULT;1.1;AES256`

mit ID label

`$ANSIBLE_VAULT;1.2;AES256;vault-id-label`

Hier ist nur _vault-id-label_ wichtig da es für die anderen werte keine alternativen gibt.

Im beispiel mit dem aufruf contrib/vault/vault-keyring-client.py --vault-id stack` würde aus _$ANSIBLE_VAULT;1.2;AES256;vault-id-label_ dann _$ANSIBLE_VAULT;1.2;AES256;stack_ abgerufen werden

### Ansible Vault

**Skripte**

files/ansible-vault-copy.sh

## Debugen

* [molecule](https://github.com/kernt/molecule)

**Quellen**

* [ansible-and-ssh-considerations](https://medium.com/linux-academy/ansible-and-ssh-considerations-cc6a4299fb75)
* [three-little-known-file-manipulation-tactics-in-ansible](https://medium.com/linux-academy/three-little-known-file-manipulation-tactics-in-ansible-c7253758bb16)
* [using-ansible-facts-to-view-system-properties](https://linuxacademy.com/blog/linux/using-ansible-facts-to-view-system-properties/)
* [configuration-management-with-ansible](https://sysadmincasts.com/episodes/46-configuration-management-with-ansible-part-3-4)