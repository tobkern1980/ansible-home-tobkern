# List of Tags and a discription

Allegemeine Beispiele

--tags all- Führt alle Aufgaben aus, markiert und ungetaggt, außer wenn never(Standardverhalten).
--tags tag1,tag2- Führt nur Aufgaben mit dem Tag tag1oder dem Tag aus tag2(auch die mit dem Tag gekennzeichneten always).
--skip-tags tag3,tag4- Führt alle Aufgaben aus, außer denen mit dem Tag tag3oder dem Tag tag4oder never.
--tags tagged- Führt nur Aufgaben mit mindestens einem Tag aus ( neverOverrides).
--tags untagged- Führt nur Aufgaben ohne Tags ( alwaysÜberschreibungen) aus.


## Spezielle Tags

Für Ansible sind einige tag namen für speciale zwecke resaviert, dazu zählen:

 - always
 - never
 - tagged
 - untagged
 - all

 Both always and never are mostly for use in tagging the tasks themselves, the other three are used when selecting which tags to run or skip.

## Tag Konvention

_setup-all_: Durchleuft all setup tasks inkusieve installation und uninstallation für alle Kompuneneten, aber managed keine start/restart services

_install-all_: it is similar to setup-all, but skips uninstallation tasks. It is useful for maintaining your setup quickly when its components remain unchanged. If you adjust your vars.yml to remove components, you’d need to run setup-all though, or these components will still remain installed.

_setup-SERVICE_ (beispiel setup-bot-postmoogle): Führt den setup task nur für role aus, but does not start/restart services.

_install-SERVICE_ (beispiel install-bot-postmoogle): like setup-SERVICE, but skips uninstallation tasks.

_start_ – starts all systemd services and makes them start automatically in the future

_stop_ – stops all systemd services

_debug_ - Um debug infos zur fehleruntersuchung zu bekommen