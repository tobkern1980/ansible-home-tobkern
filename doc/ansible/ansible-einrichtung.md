# Ansible einrichtung

**Repo einrichten**

`rm -rf ~/.ansible && git clone https://gitlab.com/tobkern1980/ansible-home-tobkern.git -o ~/.ansible`

## Initiale Einrichtung von Ansible

**Einfache role erstellen**

`ansible-galaxy init base_httpd`

**Datein in der Role Managen**

`echo 'Hello World!' > ./base_httpd/files/index.html`

**Ansible pip installation bestimmter version**

`pip install ansible==1.8.4`

## Windows installation

pip install ansible==1.8.4

### Multiversion

```sh
mkdir /opt/python/ansible ; cd /opt/python/ansible
virtualenv -p python3 /opt/python/ansible/2.8.0
```

**Ansible 2.8 Deployment**

```sh
ANSIBLE_VERSION="2.8.0"
source /opt/ansible/ansible/${ANSIBLE_VERSION}/bin/activate
pip install --upgrade pip
pip install ansible==${ANSIBLE_VERSION}
ansible --version
deactivate
```

```sh
python3.6 -m venv ansible2.7.0
python3.6 -m venv ansible2.8.0
```

Quellen:

* [run-multiple-ansible-versions-side-by-side-using-python-3-virtual-environments](https://www.simplygeek.co.uk/2019/08/23/run-multiple-ansible-versions-side-by-side-using-python-3-virtual-environments/)

* [ansible-deploying-multiple-ansible-version-on-control-node](https://learningtechnix.wordpress.com/2020/08/02/ansible-deploying-multiple-ansible-version-on-control-node/)
