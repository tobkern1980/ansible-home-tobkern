# Ansible ad-hock commands

* [ansible-modules-from-the-command-line](https://medium.com/splunkuserdeveloperadministrator/)ansible-modules-from-the-command-line-9b8da771cb9e

## Ansible Netwerk

**ansible ping**

`ansible localhost -i $(hostname -f), -m ping`

## Ansible Quellcode verwaltung

**ansible git repo**

`ansible localhost -i $(hostname -f) -m git -a “repo=’$REPO' dest=~/test/”`

**ansible git clone repo**

`ansible localhost -i $(hostname -f), -m git -a “repo=’https://github.com/vincesesto/markdown-cheatsheet.git' dest=~/test/”`

## Ansible shell benutzen

**ansible shell ls**

`ansible localhost -i $(hostname -f), -m shell -a "ls -l"`

## Ansible Webmaster

**ansible apache present**

`ansible localhost -i $(hostname -f), -m apt -a “name=apache2 state=present”`

**ansible get url**

`ansible localhost -i $(hostname -f), -m get_url -a “url=http://localhost dest=~/test/”`

## Ansible Userverwaltung

**ansible add user**

`ansible localhost -i $(hostname -f), -K -i hosts -m user -a “name=ansible comment=’Ansible system User’ state=present”`

**ansible get url**

`ansible localhost -i $(hostname -f), -m get_url -a “url=http://localhost dest=~/test/”`

**ansible get hosts ids**

`ansible all -i unmanaged-inventory.yml --extra_vars="ansible_python_interpreter=/usr/bin/python3 ansible_user=tkern ansible_password=password" -m known_hosts -a "name: {{ inventory_hostname }} state=present "`

## Ansible Datei verwaltung

**ansible file**


## Ansible Virtualle system verwalten

**vm Ststus**

`ansible host -m virt -a "name=alpha command=status"`

**vm xml Konfiguration abfragen**

`ansible host -m virt -a "name=alpha command=get_xml"`

**lxc vm erstell**

`ansible host -m virt -a "name=alpha command=create uri=lxc:///"`