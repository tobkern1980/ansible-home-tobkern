# Ansinle umgang mit Ditributionen

ansible all -m setup -a "filter=ansible_distribution*"

{{ ansible_distribution_version } or {{ ansible_distribution_release }}

```yml

    "ansible_distribution": "CentOS",
    "ansible_distribution_release": "Final",
    "ansible_distribution_version": "6.4"

    "ansible_distribution": "Ubuntu",
    "ansible_distribution_release": "precise",
    "ansible_distribution_version": "12.04"

    "ansible_distribution": "Debian",
    "ansible_distribution_release": "wheezy",
    "ansible_distribution_version": "7"

when: ansible_distribution == 'CentOS' and ansible_distribution_version == '6.4'
when: ansible_distribution == 'Ubuntu' and ansible_distribution_release == 'precise'
when: ansible_distribution == 'Debian' and ansible_distribution_version == '7'
when: ansible_os_family == "RedHat" and ansible_lsb.major_release|int >= 5
```