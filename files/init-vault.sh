#!/bin/bash
# initial Vaults Pass
# Source: 
# https://dev.iachieved.it/iachievedit/ansible-vault-ids/
FILES=[
    ~/.vault-pass.common
    ~/.vault-pass.staging
    ~/.vault-pass.production
]

for file in ${FILES[*]} do
  if [ ! -f $file ]
    tr -cd '[:alnum:]' < /dev/urandom | fold -w32 | head -n1 > $file 
  fi
done
