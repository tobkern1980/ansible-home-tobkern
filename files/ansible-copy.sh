#!/bin/bash
GROUP=$(groups | cut -d "1" -f 1)
USER="$USER"
SRC="$1"
DEST="$2"
ANSIBLE_BIN="$(type -p ansible)"
${ANSIBLE_BIN} --ask-vault -m copy -a "src=$SRC dest=$DEST mode=600 owner=$GROUP group=$GROUP" $3