# .bash_profile

# Get the functions
if [ -f ~/.bashrc ]; then
        . ~/.bashrc
fi

# User specific environment and startup programs

if [ -f ~/.bash_aliases ]; then
          . ~/.bash_aliases
fi

add-alias ()
{
   local name=$1 value="$2"
   echo alias $name=\'$value\' >>~/.bash_aliases
   eval alias $name=\'$value\'
   alias $name
}

## ansible exports 
export ANSIBLE_HOST_KEY_CHECKING=False