#!/usr/bin/env bash
# THIS FILE IS MANAGED BY ANSIBLE, DO NOT CHANGE

yum () {
    /usr/bin/yum "$@" || return

    while test ${#} -gt 0; do
        case "$1" in
            "install"|"update"|"update-to"|"upgrade"|"upgrade-to"|"groupinstall"|"groupupdate"|"localinstall"|"localupdate"|"reinstall"|"langinstall"|"update-minimal")
                info;
                return;
            ;;
            *)
            ;;
        esac
        shift
    done
}

info () {
    echo ""
    echo "==="
    echo ""
    echo "After updates/installs, please make sure to handle prelink/rkhunter:"
    echo ""
    if [ -e /etc/cron.daily/prelink ]; then
        echo "    touch /var/lib/prelink/force"
        echo "    /etc/cron.daily/prelink"
    fi
    echo "    rkhunter --propupd"
    echo "    rkhunter -c --cronjob --rwo"
    echo ""
    echo "===";
}

export -f yum
