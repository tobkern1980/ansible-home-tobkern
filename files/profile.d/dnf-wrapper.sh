#!/usr/bin/env bash
# THIS FILE IS MANAGED BY ANSIBLE, DO NOT CHANGE

dnf () {
    /usr/bin/dnf "$@" || return

    while test ${#} -gt 0; do
        case "$1" in
            "install"|"upgrade"|"upgrade-minimal"|"downgrade"|"reinstall")
                info;
                return;
            ;;
            *)
            ;;
        esac
        shift
    done
}

info () {
    echo ""
    echo "==="
    echo ""
    echo "After updates/installs, please make sure to handle prelink/rkhunter:"
    echo ""
    if [ -e /etc/cron.daily/prelink ]; then
        echo "    touch /var/lib/prelink/force"
        echo "    /etc/cron.daily/prelink"
    fi
    echo "    rkhunter --propupd"
    echo "    rkhunter -c --cronjob --rwo"
    echo ""
    echo "===";
}

export -f dnf
