#!/bin/bash

# if []; then
mkdir ./extra/reclass
# fi

# http://reclass.pantsfullofunix.net/
echo "Install Reclass"
git clone https://github.com/madduck/reclass.git ./extra/reclass

cd ./extra/reclass

python setup.py install

cd - 