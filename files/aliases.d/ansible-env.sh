export ANSIBLE_VAULT_PASSWORD_FILE=~/.vault_pass.txt
export ANSIBLE_LOG_PATH=$HOME/ansible.log
export ANSIBLE_NETCONF_SSH_CONFIG=$HOME/.ssh/ansible-config