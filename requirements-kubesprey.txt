#  
#
#VENVDIR=kubespray-venv
#KUBESPRAYDIR=kubespray
#python3 -m venv $VENVDIR
#source $VENVDIR/bin/activate
#cd $KUBESPRAYDIR
#pip install -U -r requirements-kubesprey.txt
#
ansible==9.13.0
# Needed for community.crypto module
cryptography==44.0.0
# Needed for jinja2 json_query templating
jmespath==1.0.1
# Needed for ansible.utils.ipaddr
netaddr==1.3.0


## Installes to /home/tobkern/.ansible/collections/ansible_collections/kubernetes_sigs/kubespray
# add submodule
# git submodule add https://github.com/kernt/kubespray.git /home/tobkern/.ansible/collections/ansible_collections/kubernetes_sigs/kubespray