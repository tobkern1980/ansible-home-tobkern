---
# https://docs.ansible.com/ansible/latest/collections/community/hashi_vault/hashi_vault_lookup.html#ansible-collections-community-hashi-vault-hashi-vault-lookup
# https://docs.ansible.com/ansible/latest/collections/community/hashi_vault/vault_read_lookup.html#ansible-collections-community-hashi-vault-vault-read-lookup
- name: Install aide
  hosts: all
  tasks:
  - name: Perform multiple reads with a single Vault login in a loop (via with_)
    vars:
      ansible_hashi_vault_auth_method: userpass
      ansible_hashi_vault_username: '{{ hashi-vault-user }}'
      ansible_hashi_vault_passowrd: '{{ hashi-vault-user-password }}'
    ansible.builtin.debug:
      msg: '{{ item }}'
    with_community.hashi_vault.vault_read:
      - secret/data/hello
      - auth/approle/role/role-one/role-id
      - auth/approle/role/role-two/role-id
  
  # ansible vault plugins
  
  ## https://docs.ansible.com/ansible/latest/collections/community/hashi_vault/hashi_vault_lookup.html#ansible-collections-community-hashi-vault-hashi-vault-lookup 
  - ansible.builtin.debug:
      msg: "{{ lookup('community.hashi_vault.hashi_vault', 'secret=secret/hello:value token=c975b780-d1be-8016-866b-01d0f9b688a5 url=http://myvault:8200') }}"
  
  - name: Return all secrets from a path
    ansible.builtin.debug:
      msg: "{{ lookup('community.hashi_vault.hashi_vault', 'secret=secret/hello token=c975b780-d1be-8016-866b-01d0f9b688a5 url=http://myvault:8200') }}"
  
  - name: Vault that requires authentication via LDAP
    ansible.builtin.debug:
      msg: "{{ lookup('community.hashi_vault.hashi_vault', 'secret/hello:value auth_method=ldap mount_point=ldap username=myuser password=mypas') }}"
  
  - name: Vault that requires authentication via username and password
    ansible.builtin.debug:
      msg: "{{ lookup('community.hashi_vault.hashi_vault', 'secret=secret/hola:val auth_method=userpass username=myuser password=psw url=http://vault:8200') }}"
  
  - name: Connect to Vault using TLS
    ansible.builtin.debug:
      msg: "{{ lookup('community.hashi_vault.hashi_vault', 'secret=secret/hola:value token=c975b780-d1be-8016-866b-01d0f9b688a5 validate_certs=False') }}"
  
  - name: using certificate auth
    ansible.builtin.debug:
      msg: "{{ lookup('community.hashi_vault.hashi_vault', 'secret/hi:val token=xxxx url=https://vault:8200 validate_certs=True cacert=/cacert/path/ca.pem') }}"
  
  - name: Authenticate with a Vault app role
    ansible.builtin.debug:
      msg: "{{ lookup('community.hashi_vault.hashi_vault', 'secret=secret/hello:value auth_method=approle role_id=myroleid secret_id=mysecretid') }}"
  
  - name: Return all secrets from a path in a namespace
    ansible.builtin.debug:
      msg: "{{ lookup('community.hashi_vault.hashi_vault', 'secret=secret/hello token=c975b780-d1be-8016-866b-01d0f9b688a5 namespace=teama/admins') }}"
  
  # When using KV v2 the PATH should include "data" between the secret engine mount and path (e.g. "secret/data/:path")
  # see: https://www.vaultproject.io/api/secret/kv/kv-v2.html#read-secret-version
  - name: Return latest KV v2 secret from path
    ansible.builtin.debug:
      msg: "{{ lookup('community.hashi_vault.hashi_vault', 'secret=secret/data/hello token=my_vault_token url=http://myvault_url:8200') }}"
  
  # The following examples show more modern syntax, with parameters specified separately from the term string.
  
  - name: secret= is not required if secret is first
    ansible.builtin.debug:
      msg: "{{ lookup('community.hashi_vault.hashi_vault', 'secret/data/hello token=<token> url=http://myvault_url:8200') }}"
  
  - name: options can be specified as parameters rather than put in term string
    ansible.builtin.debug:
      msg: "{{ lookup('community.hashi_vault.hashi_vault', 'secret/data/hello', token=my_token_var, url='http://myvault_url:8200') }}"
  
  # return_format (or its alias 'as') can control how secrets are returned to you
  - name: return secrets as a dict (default)
    ansible.builtin.set_fact:
      my_secrets: "{{ lookup('community.hashi_vault.hashi_vault', 'secret/data/manysecrets', token=my_token_var, url='http://myvault_url:8200') }}"
  - ansible.builtin.debug:
      msg: "{{ my_secrets['secret_key'] }}"
  - ansible.builtin.debug:
      msg: "Secret '{{ item.key }}' has value '{{ item.value }}'"
    loop: "{{ my_secrets | dict2items }}"
  
  - name: return secrets as values only
    ansible.builtin.debug:
      msg: "A secret value: {{ item }}"
    loop: "{{ query('community.hashi_vault.hashi_vault', 'secret/data/manysecrets', token=my_token_var, url='http://vault_url:8200', return_format='values') }}"
  
  - name: return raw secret from API, including metadata
    ansible.builtin.set_fact:
      my_secret: "{{ lookup('community.hashi_vault.hashi_vault', 'secret/data/hello:value', token=my_token_var, url='http://myvault_url:8200', as='raw') }}"
  - ansible.builtin.debug:
      msg: "This is version {{ my_secret['metadata']['version'] }} of hello:value. The secret data is {{ my_secret['data']['data']['value'] }}"
  
  # AWS IAM authentication method
  # uses Ansible standard AWS options
  
  - name: authenticate with aws_iam
    ansible.builtin.debug:
      msg: "{{ lookup('community.hashi_vault.hashi_vault', 'secret/hello:value', auth_method='aws_iam', role_id='myroleid', profile=my_boto_profile) }}"
  
  # JWT auth
  
  - name: Authenticate with a JWT
    ansible.builtin.debug:
      msg: "{{ lookup('community.hashi_vault.hashi_vault', 'secret/hola:val', auth_method='jwt', role_id='myroleid', jwt='myjwt', url='https://vault:8200') }}"
  
  # Disabling Token Validation
  # Use this when your token does not have the lookup-self capability. Usually this is applied to all tokens via the default policy.
  # However you can choose to create tokens without applying the default policy, or you can modify your default policy not to include it.
  # When disabled, your invalid or expired token will be indistinguishable from insufficent permissions.
  
  - name: authenticate without token validation
    ansible.builtin.debug:
      msg: "{{ lookup('community.hashi_vault.hashi_vault', 'secret/hello:value', token=my_token, token_validate=False) }}"
  
  # "none" auth method does no authentication and does not send a token to the Vault address.
  # One example of where this could be used is with a Vault agent where the agent will handle authentication to Vault.
  # https://www.vaultproject.io/docs/agent
  
  - name: authenticate with vault agent
    ansible.builtin.debug:
      msg: "{{ lookup('community.hashi_vault.hashi_vault', 'secret/hello:value', auth_method='none', url='http://127.0.0.1:8100') }}"
  
  # Use a proxy
  
  - name: use a proxy with login/password
    ansible.builtin.debug:
      msg: "{{ lookup('community.hashi_vault.hashi_vault', 'secret=... token=... url=https://... proxies=https://user:pass@myproxy:8080') }}"
  
  - name: 'use a socks proxy (need some additional dependencies, see: https://requests.readthedocs.io/en/master/user/advanced/#socks )'
    ansible.builtin.debug:
      msg: "{{ lookup('community.hashi_vault.hashi_vault', 'secret=... token=... url=https://... proxies=socks5://myproxy:1080') }}"
  
  - name: use proxies with a dict (as param)
    ansible.builtin.debug:
      msg: "{{ lookup('community.hashi_vault.hashi_vault', '...', proxies={'http': 'http://myproxy1', 'https': 'http://myproxy2'}) }}"
  
  - name: use proxies with a dict (as param, pre-defined var)
    vars:
      prox:
        http: http://myproxy1
        https: https://myproxy2
    ansible.builtin.debug:
      msg: "{{ lookup('community.hashi_vault.hashi_vault', '...', proxies=prox }}"
  
  - name: use proxies with a dict (as direct ansible var)
    vars:
      ansible_hashi_vault_proxies:
        http: http://myproxy1
        https: https://myproxy2
    ansible.builtin.debug:
      msg: "{{ lookup('community.hashi_vault.hashi_vault', '...' }}"
  
  - name: use proxies with a dict (in the term string, JSON syntax)
    ansible.builtin.debug:
      msg: "{{ lookup('community.hashi_vault.hashi_vault', '... proxies={\"http\":\"http://myproxy1\",\"https\":\"http://myproxy2\"}') }}"
  
  - name: use ansible vars to supply some options
    vars:
      ansible_hashi_vault_url: 'https://myvault:8282'
      ansible_hashi_vault_auth_method: token
    ansible.builtin.set_fact:
      secret1: "{{ lookup('community.hashi_vault.hashi_vault', 'secret/data/secret1') }}"
      secret2: "{{ lookup('community.hashi_vault.hashi_vault', 'secret/data/secret2') }}"
  
  - name: use a custom timeout
    debug:
      msg: "{{ lookup('community.hashi_vault.hashi_vault', 'secret/data/secret1', timeout=120) }}"
  
  - name: use a custom timeout and retry on failure 3 times (with collection retry defaults)
    vars:
      ansible_hashi_vault_timeout: 5
      ansible_hashi_vault_retries: 3
    debug:
      msg: "{{ lookup('community.hashi_vault.hashi_vault', 'secret/data/secret1') }}"
  
  - name: retry on failure (with custom retry settings and no warnings)
    vars:
      ansible_hashi_vault_retries:
        total: 6
        backoff_factor: 0.9
        status_forcelist: [500, 502]
        allowed_methods:
          - GET
          - PUT
    debug:
      msg: "{{ lookup('community.hashi_vault.hashi_vault', 'secret/data/secret1', retry_action='warn') }}"
  
  # https://docs.ansible.com/ansible/latest/collections/community/hashi_vault/vault_login_lookup.html#ansible-collections-community-hashi-vault-vault-login-lookup
  - name: Set a fact with a lookup result
    ansible.builtin.set_fact:
      login_data: "{{ lookup('community.hashi_vault.vault_login', url='https://vault', auth_method='userpass', username=user, password=pwd) }}"
  
  - name: Retrieve an approle role ID (token via filter)
    community.hashi_vault.vault_read:
      url: https://vault:8201
      auth_method: token
      token: '{{ login_data | community.hashi_vault.vault_login_token }}'
      path: auth/approle/role/role-name/role-id
    register: approle_id
  
  - name: Retrieve an approle role ID (token via direct dict access)
    community.hashi_vault.vault_read:
      url: https://vault:8201
      auth_method: token
      token: '{{ login_data.auth.client_token }}'
      path: auth/approle/role/role-name/role-id
    register: approle_id
  
  # https://docs.ansible.com/ansible/latest/collections/community/hashi_vault/vault_read_lookup.html#ansible-collections-community-hashi-vault-vault-read-lookup
  - name: Read a kv2 secret
    ansible.builtin.debug:
      msg: "{{ lookup('community.hashi_vault.vault_read', 'secret/data/hello', url='https://vault:8201') }}"
  
  - name: Retrieve an approle role ID
    ansible.builtin.debug:
      msg: "{{ lookup('community.hashi_vault.vault_read', 'auth/approle/role/role-name/role-id', url='https://vault:8201') }}"
  
  - name: Perform multiple reads with a single Vault login
    vars:
      paths:
        - secret/data/hello
        - auth/approle/role/role-one/role-id
        - auth/approle/role/role-two/role-id
    ansible.builtin.debug:
      msg: "{{ lookup('community.hashi_vault.vault_read', *paths, auth_method='userpass', username=user, password=pwd) }}"
  
  - name: Perform multiple reads with a single Vault login in a loop
    vars:
      paths:
        - secret/data/hello
        - auth/approle/role/role-one/role-id
        - auth/approle/role/role-two/role-id
    ansible.builtin.debug:
      msg: '{{ item }}'
    loop: "{{ query('community.hashi_vault.vault_read', *paths, auth_method='userpass', username=user, password=pwd) }}"
  
  - name: Perform multiple reads with a single Vault login in a loop (via with_)
    vars:
      ansible_hashi_vault_auth_method: userpass
      ansible_hashi_vault_username: '{{ user }}'
      ansible_hashi_vault_password: '{{ pwd }}'
    ansible.builtin.debug:
      msg: '{{ item }}'
    with_community.hashi_vault.vault_read:
      - secret/data/hello
      - auth/approle/role/role-one/role-id
      - auth/approle/role/role-two/role-id
  
  # https://docs.ansible.com/ansible/latest/collections/community/hashi_vault/vault_write_lookup.html#ansible-collections-community-hashi-vault-vault-write-lookup
  
   These examples show some uses that might work well as a lookup.
  # For most uses, the vault_write module should be used.
  
  - name: Retrieve and display random data
    vars:
      data:
        format: hex
      num_bytes: 64
    ansible.builtin.debug:
      msg: "{{ lookup('community.hashi_vault.vault_write', 'sys/tools/random/' ~ num_bytes, data=data) }}"
  
  - name: Hash some data and display the hash
    vars:
      input: |
        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Pellentesque posuere dui a ipsum dapibus, et placerat nibh bibendum.
      data:
        input: '{{ input | b64encode }}'
      hash_algo: sha2-256
    ansible.builtin.debug:
      msg: "The hash is {{ lookup('community.hashi_vault.vault_write', 'sys/tools/hash/' ~ hash_algo, data=data) }}"
  
  # In this next example, the Ansible controller's token does not have permission to read the secrets we need.
  # It does have permission to generate new secret IDs for an approle which has permission to read the secrets,
  # however the approle is configured to:
  # 1) allow a maximum of 1 use per secret ID
  # 2) restrict the IPs allowed to use login using the approle to those of the remote hosts
  #
  # Normally, the fact that a new secret ID would be generated on every loop iteration would not be desirable,
  # but here it's quite convenient.
  
  - name: Retrieve secrets from the remote host with one-time-use approle creds
    vars:
      role_id: "{{ lookup('community.hashi_vault.vault_read', 'auth/approle/role/role-name/role-id') }}"
      secret_id: "{{ lookup('community.hashi_vault.vault_write', 'auth/approle/role/role-name/secret-id') }}"
    community.hashi_vault.vault_read:
      auth_method: approle
      role_id: '{{ role_id }}'
      secret_id: '{{ secret_id }}'
      path: '{{ item }}'
    register: secret_data
    loop:
      - secret/data/secret1
      - secret/data/app/deploy-key
      - secret/data/access-codes/self-destruct
  
  
  # This time we have a secret values on the controller, and we need to run a command the remote host,
  # that is expecting to a use single-use token as input, so we need to use wrapping to send the data.
  
  - name: Run a command that needs wrapped secrets
    vars:
      secrets:
        secret1: '{{ my_secret_1 }}'
        secret2: '{{ second_secret }}'
      wrapped: "{{ lookup('community.hashi_vault.vault_write', 'sys/wrapping/wrap', data=secrets) }}"
    ansible.builtin.command: 'vault unwrap {{ wrapped }}'
  
  # https://docs.ansible.com/ansible/latest/collections/community/hashi_vault/vault_token_create_lookup.html#ansible-collections-community-hashi-vault-vault-token-create-lookup
  
  - name: Login via userpass and create a child token
    ansible.builtin.set_fact:
      token_data: "{{ lookup('community.hashi_vault.vault_token_create', url='https://vault', auth_method='userpass', username=user, password=passwd) }}"
  
  - name: Retrieve an approle role ID using the child token (token via filter)
    community.hashi_vault.vault_read:
      url: https://vault:8201
      auth_method: token
      token: '{{ token_data | community.hashi_vault.vault_login_token }}'
      path: auth/approle/role/role-name/role-id
    register: approle_id
  
  - name: Retrieve an approle role ID (token via direct dict access)
    community.hashi_vault.vault_read:
      url: https://vault:8201
      auth_method: token
      token: '{{ token_data.auth.client_token }}'
      path: auth/approle/role/role-name/role-id
    register: approle_id
  
  # implicitly uses url & token auth with a token from the environment
  - name: Create an orphaned token with a short TTL and display the full response
    ansible.builtin.debug:
      var: lookup('community.hashi_vault.vault_token_create', orphan=True, ttl='60s')
  
  
  # https://docs.ansible.com/ansible/latest/collections/community/hashi_vault/vault_login_module.html#ansible-collections-community-hashi-vault-vault-login-module
  - name: Login and use the resulting token
    community.hashi_vault.vault_login:
      url: https://vault:8201
      auth_method: userpass
      username: user
      password: '{{ passwd }}'
    register: login_data
  
  - name: Retrieve an approle role ID (token via filter)
    community.hashi_vault.vault_read:
      url: https://vault:8201
      auth_method: token
      token: '{{ login_data | community.hashi_vault.vault_login_token }}'
      path: auth/approle/role/role-name/role-id
    register: approle_id
  
  - name: Retrieve an approle role ID (token via direct dict access)
    community.hashi_vault.vault_read:
      url: https://vault:8201
      auth_method: token
      token: '{{ login_data.login.auth.client_token }}'
      path: auth/approle/role/role-name/role-id
    register: approle_id
  
  
  - name: Login and use the resulting token
    community.hashi_vault.vault_login:
      url: https://localhost:8200
      auth_method: ldap
      username: "john.doe"
      password: "{{ user_passwd }}"
    register: login_data
  
  #https://docs.ansible.com/ansible/latest/collections/community/hashi_vault/vault_pki_generate_certificate_module.html#ansible-collections-community-hashi-vault-vault-pki-generate-certificate-module
  - name: Generate a certificate with an existing token
    community.hashi_vault.vault_pki_generate_certificate:
      role_name: test.example.org
      common_name: test.example.org
      ttl: 8760h
      alt_names:
        - test2.example.org
        - test3.example.org
      url: https://vault:8201
      auth_method: token
      token: "{{ login_data.login.auth.client_token }}"
    register: cert_data
  
  - name: Display generated certificate
    debug:
      msg: "{{ cert_data.data.data.certificate }}"
  
  # https://docs.ansible.com/ansible/latest/collections/community/hashi_vault/vault_read_module.html#ansible-collections-community-hashi-vault-vault-read-module
  
  - name: Read a kv2 secret from Vault via the remote host with userpass auth
    community.hashi_vault.vault_read:
      url: https://vault:8201
      path: secret/data/hello
      auth_method: userpass
      username: user
      password: '{{ passwd }}'
    register: secret
  
  - name: Display the secret data
    ansible.builtin.debug:
      msg: "{{ secret.data.data.data }}"
  
  - name: Retrieve an approle role ID from Vault via the remote host
    community.hashi_vault.vault_read:
      url: https://vault:8201
      path: auth/approle/role/role-name/role-id
    register: approle_id
  
  - name: Display the role ID
    ansible.builtin.debug:
      msg: "{{ approle_id.data.data.role_id }}"
  
  #https://docs.ansible.com/ansible/latest/collections/community/hashi_vault/vault_token_create_module.html#ansible-collections-community-hashi-vault-vault-token-create-module
  
  - name: Login via userpass and create a child token
    community.hashi_vault.vault_token_create:
      url: https://vault:8201
      auth_method: userpass
      username: user
      password: '{{ passwd }}'
    register: token_data
  
  - name: Retrieve an approle role ID using the child token (token via filter)
    community.hashi_vault.vault_read:
      url: https://vault:8201
      auth_method: token
      token: '{{ token_data | community.hashi_vault.vault_login_token }}'
      path: auth/approle/role/role-name/role-id
    register: approle_id
  
  - name: Retrieve an approle role ID using the child token (token via direct dict access)
    community.hashi_vault.vault_read:
      url: https://vault:8201
      auth_method: token
      token: '{{ token_data.login.auth.client_token }}'
      path: auth/approle/role/role-name/role-id
    register: approle_id
  
  # implicitly uses token auth with a token from the environment
  - name: Create an orphaned token with a short TTL
    community.hashi_vault.vault_token_create:
      url: https://vault:8201
      orphan: true
      ttl: 60s
    register: token_data
  
  - name: Display the full response
    ansible.builtin.debug:
      var: token_data.login
  
  #https://docs.ansible.com/ansible/latest/collections/community/hashi_vault/vault_write_module.html#ansible-collections-community-hashi-vault-vault-write-module
  - name: Write a value to the cubbyhole via the remote host with userpass auth
    community.hashi_vault.vault_write:
      url: https://vault:8201
      path: cubbyhole/mysecret
      data:
        key1: val1
        key2: val2
      auth_method: userpass
      username: user
      password: '{{ passwd }}'
    register: result
  
  - name: Display the result of the write (this can be empty)
    ansible.builtin.debug:
      msg: "{{ result.data }}"
  
  - name: Retrieve an approle role ID from Vault via the remote host
    community.hashi_vault.vault_read:
      url: https://vault:8201
      path: auth/approle/role/role-name/role-id
    register: approle_id
  
  - name: Generate a secret-id for the given approle
    community.hashi_vault.vault_write:
      url: https://vault:8201
      path: auth/approle/role/role-name/secret-id
    register: secret_id
  
  - name: Display the role ID and secret ID
    ansible.builtin.debug:
      msg:
        - "role-id: {{ approle_id.data.data.role_id }}"
        - "secret-id: {{ secret_id.data.data.secret_id }}"
  