---
#
- hosts: localhost
  gather_facts: false
  tasks:
    - name: Tear down existing services
      community.docker.docker_compose:
        project_src: flask
        state: absent

   - name: Create and start services
     community.docker.docker_compose:
       project_src: flask
     register: output

   - name: Show results
     ansible.builtin.debug:
       var: output

  - name: Run `docker-compose up` again
    community.docker.docker_compose:
      project_src: flask
      build: false
    register: output

  - name: Show results
    ansible.builtin.debug:
      var: output

  - ansible.builtin.assert:
      that: not output.changed

  - name: Stop all services
    community.docker.docker_compose:
      project_src: flask
      build: false
      stopped: true
    register: output

  - name: Show results
    ansible.builtin.debug:
      var: output

  - name: Verify that web and db services are not running
    ansible.builtin.assert:
      that:
        - "not output.services.web.flask_web_1.state.running"
        - "not output.services.db.flask_db_1.state.running"

  - name: Restart services
    community.docker.docker_compose:
      project_src: flask
      build: false
      restarted: true
    register: output

  - name: Show results
    ansible.builtin.debug:
      var: output

  - name: Verify that web and db services are running
    ansible.builtin.assert:
      that:
        - "output.services.web.flask_web_1.state.running"
        - "output.services.db.flask_db_1.state.running"

  - name: Scale the web service to 2
    hosts: localhost
    gather_facts: false
    tasks:
      - name: Scale the web service to two instances
        community.docker.docker_compose:
          project_src: flask
          scale:
            web: 2
        register: output

  - name: Show results
    ansible.builtin.debug:
      var: output

- name: Run with inline Compose file version 2
  # https://docs.docker.com/compose/compose-file/compose-file-v2/
  hosts: localhost
  gather_facts: false
  tasks:
    - name: Remove flask project
      community.docker.docker_compose:
        project_src: flask
        state: absent

    - name: Start flask project with inline definition
      community.docker.docker_compose:
        project_name: flask
        definition:
          version: '2'
          services:
            db:
              image: postgres
            web:
              build: "{{ playbook_dir }}/flask"
              command: "python manage.py runserver 0.0.0.0:8000"
              volumes:
                - "{{ playbook_dir }}/flask:/code"
              ports:
                - "8000:8000"
              depends_on:
                - db
      register: output

    - name: Show results
      ansible.builtin.debug:
        var: output

    - name: Verify that the db and web services are running
      ansible.builtin.assert:
        that:
          - "output.services.web.flask_web_1.state.running"
          - "output.services.db.flask_db_1.state.running"

- name: Run with inline Compose file version 1
  # https://docs.docker.com/compose/compose-file/compose-file-v1/
  hosts: localhost
  gather_facts: false
  tasks:
    - name: Remove flask project
      community.docker.docker_compose:
        project_src: flask
        state: absent

    - name: Start flask project with inline definition
      community.docker.docker_compose:
        project_name: flask
        definition:
            db:
              image: postgres
            web:
              build: "{{ playbook_dir }}/flask"
              command: "python manage.py runserver 0.0.0.0:8000"
              volumes:
                - "{{ playbook_dir }}/flask:/code"
              ports:
                - "8000:8000"
              links:
                - db
      register: output

    - name: Show results
      ansible.builtin.debug:
        var: output

    - name: Verify that web and db services are running
      ansible.builtin.assert:
        that:
          - "output.services.web.flask_web_1.state.running"
          - "output.services.db.flask_db_1.state.running"

- name: Get facts on current container
  community.docker.current_container_facts:

- name: Print information on current container when running in a container
  ansible.builtin.debug:
    msg: "Container ID is {{ ansible_module_container_id }}"
  when: ansible_module_running_in_container

- name: Tear down existing services
      community.docker.docker_compose_v2:
        project_src: flask
        state: absent

    - name: Create and start services
      community.docker.docker_compose_v2:
        project_src: flask
      register: output

    - name: Show results
      ansible.builtin.debug:
        var: output

    - name: Run `docker-compose up` again
      community.docker.docker_compose_v2:
        project_src: flask
      register: output

    - name: Show results
      ansible.builtin.debug:
        var: output

    - ansible.builtin.assert:
        that: not output.changed

    - name: Stop all services
      community.docker.docker_compose_v2:
        project_src: flask
        state: stopped
      register: output

    - name: Show results
      ansible.builtin.debug:
        var: output

    - name: Verify that web and db services are not running
      ansible.builtin.assert:
        that:
          - "not output.services.web.flask_web_1.state.running"
          - "not output.services.db.flask_db_1.state.running"

    - name: Restart services
      community.docker.docker_compose_v2:
        project_src: flask
        state: restarted
      register: output

    - name: Show results
      ansible.builtin.debug:
        var: output

    - name: Verify that web and db services are running
      ansible.builtin.assert:
        that:
          - "output.services.web.flask_web_1.state.running"
          - "output.services.db.flask_db_1.state.running"