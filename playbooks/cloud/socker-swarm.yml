---
- name: Get info on Docker Swarm
  community.docker.docker_swarm_info:
  ignore_errors: true
  register: result

- name: Inform about basic flags
  ansible.builtin.debug:
    msg: |
      Was able to talk to docker daemon: {{ result.can_talk_to_docker }}
      Docker in Swarm mode: {{ result.docker_swarm_active }}
      This is a Manager node: {{ result.docker_swarm_manager }}

- name: Get info on Docker Swarm and list of registered nodes
  community.docker.docker_swarm_info:
    nodes: true
  register: result

- name: Get info on Docker Swarm and extended list of registered nodes
  community.docker.docker_swarm_info:
    nodes: true
    verbose_output: true
  register: result

- name: Get info on Docker Swarm and filtered list of registered nodes
  community.docker.docker_swarm_info:
    nodes: true
    nodes_filters:
      name: mynode
  register: result

- name: Show swarm facts
  ansible.builtin.debug:
    var: result.swarm_facts

- name: Get the swarm unlock key
  community.docker.docker_swarm_info:
    unlock_key: true
  register: result

- name: Print swarm unlock key
  ansible.builtin.debug:
    var: result.swarm_unlock_key

- name: Init a new swarm with default parameters
  community.docker.docker_swarm:
    state: present

- name: Update swarm configuration
  community.docker.docker_swarm:
    state: present
    election_tick: 5

- name: Add nodes
  community.docker.docker_swarm:
    state: join
    advertise_addr: 192.168.1.2
    join_token: SWMTKN-1--xxxxx
    remote_addrs: [ '192.168.1.1:2377' ]

- name: Leave swarm for a node
  community.docker.docker_swarm:
    state: absent

- name: Remove a swarm manager
  community.docker.docker_swarm:
    state: absent
    force: true

- name: Remove node from swarm
  community.docker.docker_swarm:
    state: remove
    node_id: mynode

- name: Init a new swarm with different data path interface
  community.docker.docker_swarm:
    state: present
    advertise_addr: eth0
    data_path_addr: ens10

- name: Init a new swarm with a different data path port
  community.docker.docker_swarm:
    state: present
    data_path_port: 9789

- name: Set command and arguments
  community.docker.docker_swarm_service:
    name: myservice
    image: alpine
    command: sleep
    args:
      - "3600"

- name: Set a bind mount
  community.docker.docker_swarm_service:
    name: myservice
    image: alpine
    mounts:
      - source: /tmp/
        target: /remote_tmp/
        type: bind

- name: Set service labels
  community.docker.docker_swarm_service:
    name: myservice
    image: alpine
    labels:
      com.example.description: "Accounting webapp"
      com.example.department: "Finance"

- name: Set environment variables
  community.docker.docker_swarm_service:
    name: myservice
    image: alpine
    env:
      ENVVAR1: envvar1
      ENVVAR2: envvar2
    env_files:
      - envs/common.env
      - envs/apps/web.env

- name: Set fluentd logging
  community.docker.docker_swarm_service:
    name: myservice
    image: alpine
    logging:
      driver: fluentd
      options:
        fluentd-address: "127.0.0.1:24224"
        fluentd-async-connect: "true"
        tag: myservice

- name: Set restart policies
  community.docker.docker_swarm_service:
    name: myservice
    image: alpine
    restart_config:
      condition: on-failure
      delay: 5s
      max_attempts: 3
      window: 120s

- name: Set update config
  community.docker.docker_swarm_service:
    name: myservice
    image: alpine
    update_config:
      parallelism: 2
      delay: 10s
      order: stop-first

- name: Set rollback config
  community.docker.docker_swarm_service:
    name: myservice
    image: alpine
    update_config:
      failure_action: rollback
    rollback_config:
      parallelism: 2
      delay: 10s
      order: stop-first

- name: Set placement preferences
  community.docker.docker_swarm_service:
    name: myservice
    image: alpine:edge
    placement:
      preferences:
        - spread: node.labels.mylabel
      constraints:
        - node.role == manager
        - engine.labels.operatingsystem == ubuntu 14.04
      replicas_max_per_node: 2

- name: Set configs
  community.docker.docker_swarm_service:
    name: myservice
    image: alpine:edge
    configs:
      - config_name: myconfig_name
        filename: "/tmp/config.txt"

- name: Set networks
  community.docker.docker_swarm_service:
    name: myservice
    image: alpine:edge
    networks:
      - mynetwork

- name: Set networks as a dictionary
  community.docker.docker_swarm_service:
    name: myservice
    image: alpine:edge
    networks:
      - name: "mynetwork"
        aliases:
          - "mynetwork_alias"
        options:
          foo: bar

- name: Set secrets
  community.docker.docker_swarm_service:
    name: myservice
    image: alpine:edge
    secrets:
      - secret_name: mysecret_name
        filename: "/run/secrets/secret.txt"

- name: Start service with healthcheck
  community.docker.docker_swarm_service:
    name: myservice
    image: nginx:1.13
    healthcheck:
      # Check if nginx server is healthy by curl'ing the server.
      # If this fails or timeouts, the healthcheck fails.
      test: ["CMD", "curl", "--fail", "http://nginx.host.com"]
      interval: 1m30s
      timeout: 10s
      retries: 3
      start_period: 30s

- name: Configure service resources
  community.docker.docker_swarm_service:
    name: myservice
    image: alpine:edge
    reservations:
      cpus: 0.25
      memory: 20M
    limits:
      cpus: 0.50
      memory: 50M

- name: Remove service
  community.docker.docker_swarm_service:
    name: myservice
    state: absent

- name: Get info from a service
  community.docker.docker_swarm_service_info:
    name: myservice
  register: result