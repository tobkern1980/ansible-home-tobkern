#!/bin/bash
PROVISION_PATH=~/vagrant-ansible/provisioning
ROLE_PATH=${PROVISION_PATH}/roles/hello_webmkdir -p ${ROLE_PATH}/{defaults,files,tasks}
TEST_PATH=${ROLE_PATH}/test/integration/default/testinfra

touch ${ROLE_PATH}/{defaults,tasks}/main.yml  \
  ${PROVISION_PATH}/playbook.yml

mkdir -p ${TEST_PATH}
touch ${TEST_PATH}/test_default.py

