# vagrant/config.rb
$instance_name_prefix = "kub"
$vm_cpus = 1
$num_instances = 3
$os = "centos8-bento"
$subnet = "10.0.20"
$network_plugin = "flannel"

$extra_vars = {
    dns_domain: mykubecluster2.cluster.local
}
# or
$extra_vars = "./vagrant/vars/file.yml"