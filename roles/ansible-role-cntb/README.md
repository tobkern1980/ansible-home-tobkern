# Ansible Role for cntb

**Requirements**


Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required.

**Role Variables**

A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

-oauth2-clientid=(Your client ID)
--oauth2-client-secret=(You have too create them .Example djtz1bba-1111-2222-3333-8db823de6dde)
--oauth2-user=(Your WebPage User LoginName)
--oauth2-password=(Your WebPage User Password)

**Dependencies**


## Example Playbook


Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

```yaml
    - hosts: servers
      roles:
         - { role: kernt.ansible-role-cntb, x: 42 }
```

`cntb config set-credentials --oauth2-clientid=<ClientId from Customer Control Panel> --oauth2-client-secret=<ClientSecret from Customer Control Panel> --oauth2-user=<API User from Customer Control Panel> --oauth2-password=<API Password from Customer Control Panel>`

Available Commands:
  assign                  Add instance to private network
  cancel                  Cancel an existing subscription
  completion              Generate completion script
  config                  Manage config files
  create                  Create a new resource
  delete                  Deletes a resource
  edit                    Edits an existing resource
  generateSecret          Generate a secret for resources
  get                     Show one or more resources
  help                    Help about any command
  history                 Shows history of your resources
  regenerate              regenerate specific resource
  reinstall               Reinstall an existing instance
  rescue                  rescue an existing instance
  resendEmailVerification Resend email verification
  resetPassword           Send password reset email
  resize                  Resize an existing object storage.
  restart                 Restarts a resource
  rollback                Rollback a resource
  shutdown                Shutdown a resource
  start                   Start a resource
  stats                   get stats of an resource
  stop                    Stop a resource
  unassign                Remove instance from private network
  update                  Updates an existing resource
  upgrade                 upgrade an existing instance.
  version                 Shows the version and exits



cntb get Command

  buckets          All about your buckets.
  datacenters      get all datacenters
  image            Info about a specific image
  images           All about your images
  images-stats     Info about custom images
  instance         Info about a specific instance.
  instances        All about your instances.
  object           Download a S3 object(s).
  objectStorage    Get a specific object storage.
  objectStorages   All about your object storages.
  objects          lists S3 objects in a bucket.
  permissions      All about the available servies and actions allowed.
  privateNetwork   Info about a specific privateNetwork
  privateNetworks  All about your private networks.
  role             Info about a specific role
  roles            All about your role for a specific type
  secret           Info about a specific secret
  secrets          All about your secrets
  snapshot         Info about a specific snapshot
  snapshots        All about your snapshots
  tag              Info about a specific tag
  tagAssignment    Info about a assignments of a specif tag
  tagAssignments   List all assignments for specific tag.
  tags             All about your tags
  user             Info about a specific user
  user-credentials Retrieve user credentials to access to Object Storage.
  users            list all your users

cntb get secrets
  SECRETID  NAME                           TYPE
  104655    vmd134720 user root            password  
  104656    vmd134720 user root SSHPubKey  ssh

cntb get secret 104656

cntb get images

4efbc0ba-2313-4fe1-842a-516f8652e729  debian-12                     267     267             Linux    12
d64d5c6c-9dda-4e38-8174-0ee282474d8a  ubuntu-24.04                  578     578             Linux    24.04
8b7c9b2a-ca59-48a2-92ea-5180779183cc  centos-9-stream               1140    1140            Linux    9-stream
fe6c2c36-031e-4474-aa5c-c5297196c80e  rockylinux-9                  576     576             Linux    9

cntb reinstall instance --imageId=4efbc0ba-2313-4fe1-842a-516f8652e729 --sshKeys --userData
