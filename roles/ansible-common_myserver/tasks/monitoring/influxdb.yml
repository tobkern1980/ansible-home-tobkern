---
# https://www.influxdata.com/blog/deploying-influxdb-with-ansible/
- hosts: influxdb
  tasks:
    - name: Import InfluxDB GPG signing key
      apt_key: url=https://repos.influxdata.com/influxdb.key state=present

    - name: Add InfluxDB repository
      apt_repository: repo='deb https://repos.influxdata.com/ubuntu xenial stable' state=present

    - name: Install InfluxDB packages
      apt: name=influxdb state=present

    - name: Modify InfluxDB hostname
      replace:
        dest=/etc/influxdb/influxdb.conf
        regexp='^# hostname = "localhost"$'
        replace='hostname = \"{{ ansible_hostname }}\"'
        backup=yes

    - name: Start the InfluxDB service
      service: name=influxdb state=restarted enabled=yes

    - name: Pause for InfluxDB service
      pause: seconds=3

    - name: Create sample database
      command: /usr/bin/influx -execute 'CREATE DATABASE sample_database'
      ignore_errors: yes

    - name: Load some test data into sample database
      uri:
        url: http://{{ ansible_hostname }}:8086/write?db=sample_database
        method: POST
        body: "random_ints,host=server_{{ 10 | random }} value={{ 100 | random }}"
        status_code: 204
      with_sequence: start=1 end=10

# https://docs.ansible.com/ansible/latest/collections/community/general/influxdb_database_module.html#ansible-collections-community-general-influxdb-database-module
#
# Example influxdb_database command from Ansible Playbooks
- name: Create database
  community.general.influxdb_database:
      hostname: "{{influxdb_ip_address}}"
      database_name: "{{influxdb_database_name}}"

- name: Destroy database
  community.general.influxdb_database:
      hostname: "{{influxdb_ip_address}}"
      database_name: "{{influxdb_database_name}}"
      state: absent

- name: Create database using custom credentials
  community.general.influxdb_database:
      hostname: "{{influxdb_ip_address}}"
      username: "{{influxdb_username}}"
      password: "{{influxdb_password}}"
      database_name: "{{influxdb_database_name}}"
      ssl: true
      validate_certs: true

# https://docs.ansible.com/ansible/latest/collections/community/general/influxdb_user_module.html#ansible-collections-community-general-influxdb-user-module
- name: Create a user on localhost using default login credentials
  community.general.influxdb_user:
    user_name: john
    user_password: s3cr3t

- name: Create a user on localhost using custom login credentials
  community.general.influxdb_user:
    user_name: john
    user_password: s3cr3t
    login_username: "{{ influxdb_username }}"
    login_password: "{{ influxdb_password }}"

- name: Create an admin user on a remote host using custom login credentials
  community.general.influxdb_user:
    user_name: john
    user_password: s3cr3t
    admin: true
    hostname: "{{ influxdb_hostname }}"
    login_username: "{{ influxdb_username }}"
    login_password: "{{ influxdb_password }}"

- name: Create a user on localhost with privileges
  community.general.influxdb_user:
    user_name: john
    user_password: s3cr3t
    login_username: "{{ influxdb_username }}"
    login_password: "{{ influxdb_password }}"
    grants:
      - database: 'collectd'
        privilege: 'WRITE'
      - database: 'graphite'
        privilege: 'READ'

- name: Destroy a user using custom login credentials
  community.general.influxdb_user:
    user_name: john
    login_username: "{{ influxdb_username }}"
    login_password: "{{ influxdb_password }}"
    state: absent