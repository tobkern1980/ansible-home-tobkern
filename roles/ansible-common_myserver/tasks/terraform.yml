---
# https://github.com/ivansible/devel/blob/main/roles/terraform/tasks/main.yml
# https://docs.ansible.com/ansible/latest/collections/community/general/terraform_module.html

# For community.general.github_release install:
#   ansible-galaxy collection install community.general
# To use it in a playbook, specify: community.general.github_release

- name: detect latest terraform release
  community.general.github_release:
    repository: hashicorp/terraform
    release: "{{ terraform_version }}"
    strip_v: true
    template: https://releases.hashicorp.com/terraform/{ver}/terraform_{ver}_linux_amd64.zip
    creates: "{{ terraform_binary }}"
    reinstall: "{{ terraform_upgrade |bool }}"
  register: _terraform_release

- name: download archived terraform binary
  ansible.builtin.unarchive:
    remote_src: true
    src: "{{ terraform_release.url }}"
    dest: "{{ terraform_binary |dirname }}"
  when: _terraform_release is changed
  ## fix intermittent network failures
  register: _terraform_download
  until: _terraform_download.msg |default('') is not search('timed out')


## configure permitted users for terraform

- name: configure terraform aliases (optional)
  ansible.builtin.lineinfile:
    path: ~/.local/bashrc/4alias.sh
    line: "alias {{ item }}"
  ## skip this step if ivansible.dev_user wasn't installed
  failed_when: false
  loop:
    - "tf='terraform'"
    - "tfup='terraform apply -auto-approve'"
    - "tfdown='terraform destroy -auto-approve'"
    - "tfls='terraform state list'"
  tags: dev_terraform_aliases

- name: bash completion for terraform
  block:
    - name: user-local completion for terraform (will install globally if it fails)
      ansible.builtin.lineinfile:
        path: ~/.local/bashrc/5completion.sh
        line: "complete -C {{ terraform_binary }} terraform"
      become: false
  rescue:
    ## fall back if ivansible.dev_user wasn't installed
    - name: system-global bash completion for terraform
      ansible.builtin.copy:
        dest: /etc/profile.d/completion_terraform.sh
        content: |
          # bash completion for HashiCorp Terraform
          # ansible-managed
          if [ -n "${BASH_VERSION-}" -a -n "${PS1-}" -a -n "${BASH_COMPLETION_VERSINFO-}" ]; then
              complete -C {{ terraform_binary }} terraform
          fi
        mode: 0644
      become: true
  tags: dev_terraform_completion


- name: checkout terraform plans, if permitted
  ansible.builtin.git:
    dest: "{{ terraform_plans_dir }}"
    repo: "{{ terraform_plans_repo }}"
    version: "{{ terraform_plans_branch }}"
    ## just clone, don't choke on pending modifications
    update: false
    accept_hostkey: true
  when: terraform_plans_repo != '' and 'permitted' in group_names  # noqa 602
  tags: dev_terraform_plans

- name: setup terraform enterprise (free) token, if permitted
  ansible.builtin.blockinfile:
    path: ~/.terraformrc
    create: true
    mode: 0600
    block: |
      credentials "app.terraform.io" {
        token = "{{ terraform_api_token }}"
      }
      plugin_cache_dir   = "$HOME/.terraform.d/plugin-cache"
      disable_checkpoint = true
    marker: "# == {mark} terraform enterprise (free) =="
  when: terraform_api_token != '' and is_permitted |bool  # noqa 602
  tags: dev_terraform_api_token

## terraform plugins
- name: install plugin terraform-provider-vultr
  ansible.builtin.unarchive:
    remote_src: true
    src: "{{ release_url }}/{{ file_name }}_linux_amd64.tar.gz"
    dest: "{{ terraform_plugin_dir }}"
    exclude:
      - LICENSE
      - README.md
    creates: "{{ terraform_upgrade_plugins |bool |ternary(omit, file_path) }}"
    ## perform operations as root - workaround for unarchive issue in docker
    ## see https://github.com/ansible/ansible/issues/49284
    owner: "{{ ansible_user_id }}"
    group: "{{ ansible_user_gid |string }}"
    mode: 0755
  become: true
  ## temporary variables to reduce clutter
  vars:
    repo_url: "https://github.com/{{ terraform_provider_vultr_repo_owner }}/terraform-provider-vultr"
    release_url: "{{ repo_url }}/releases/download/v{{ terraform_provider_vultr_version }}"
    file_name: "terraform-provider-vultr_v{{ terraform_provider_vultr_version }}"
    file_path: "{{ terraform_plugin_dir }}/{{ file_name }}"
  ## fix intermittent network failures
  register: _terraform_vultr_download
  until: _terraform_vultr_download.msg |d('') is not search('timed out')
  when: terraform_provider_vultr_repo_owner |d('')

- name: install plugin terraform-provisioner-ansible (from tarball)
  ansible.builtin.unarchive:
    remote_src: true
    src: "{{ release_url }}/{{ file_name }}_linux_amd64.zip"
    dest: "{{ terraform_plugin_dir }}"
    exclude:
      - LICENSE
      - README.md
    creates: "{{ terraform_upgrade_plugins |bool |ternary(omit, file_path) }}"
    ## perform operations as root - workaround for unarchive issue in docker
    ## see https://github.com/ansible/ansible/issues/49284
    owner: "{{ ansible_user_id }}"
    group: "{{ ansible_user_gid |string }}"
    mode: 0755
  become: true
  ## temporary variables to reduce clutter
  vars:
    repo_url: "https://github.com/{{ terraform_provisioner_ansible_repo_owner }}/terraform-provisioner-ansible"
    release_url: "{{ repo_url }}/releases/download/v{{ terraform_provisioner_ansible_version }}"
    file_name: "terraform-provisioner-ansible_v{{ terraform_provisioner_ansible_version }}"
    file_path: "{{ terraform_plugin_dir }}/{{ file_name }}"
  ## fix intermittent network failures
  register: _terraform_ansible_download
  until: _terraform_ansible_download.msg |default('') is not search('timed out')
  ## choose between tarball and binary
  when:
    - terraform_provisioner_ansible_is_tarball |bool
    - terraform_provisioner_ansible_repo_owner |d('')

- name: install plugin terraform-provisioner-ansible (as binary)
  ansible.builtin.get_url:
    url: "{{ release_url }}/{{ web_file_name }}"
    dest: "{{ terraform_plugin_dir }}/{{ disk_file_name }}"
    mode: 0755
    force: "{{ terraform_upgrade_plugins |bool }}"
  vars:
    repo_url: "https://github.com/{{ terraform_provisioner_ansible_repo_owner }}/terraform-provisioner-ansible"
    release_url: "{{ repo_url }}/releases/download/v{{ terraform_provisioner_ansible_version }}"
    web_file_name: "terraform-provisioner-ansible-linux-amd64_v{{ terraform_provisioner_ansible_version }}"
    disk_file_name: "terraform-provisioner-ansible_v{{ terraform_provisioner_ansible_version }}"
  when:
    - not terraform_provisioner_ansible_is_tarball |bool
    - terraform_provisioner_ansible_repo_owner |d('')