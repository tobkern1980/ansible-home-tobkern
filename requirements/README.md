# Install all roles and collections

**Ansible roles requirements**

`ansible-galaxy install -r requirements.yml`

**Ansible Galaxy collections**

`ansible-galaxy install -r requirements.yml`
