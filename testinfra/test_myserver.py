# BAD: If the test fails on nginx, python is not tested
def test_packages(host):
    for name, version in (
        ("nginx", "1.6"),
        ("python", "2.7"),
    ):
        pkg = host.package(name)
        assert pkg.is_installed
        assert pkg.version.startswith(version)

def test_passwd_file(host):
    passwd = host.file("/etc/passwd")
    assert passwd.contains("root")
    assert passwd.user == "root"
    assert passwd.group == "root"
    assert passwd.mode == 0o644

def test_cickpit_is_installed(host):
    cockpit = host.package("cockpit")
    assert cockpit.is_installed
    #assert cockpit.version.startswith("1.2")

def test_cockpit_running_and_enabled(host):
    cockpit = host.service("cockpit")
    assert cockpit.is_running
    assert cockpit.is_enabled