---
# Directory to store WireGuard configuration on the remote hosts

wireguard_remote_directory: "/etc/wireguard"

# The default port WireGuard will listen if not specified otherwise.
wireguard_port: "55002"

# The default interface name that wireguard should use if not specified otherwise.
wireguard_interface: "wg0"

# AllowedIPs = 192.168.228.10, 192.168.223.0/24, 192.168.224.0/24, 192.168.225.0/24
# Endpoint = 62.4.81.102:35661

#wireguard_postup:
#- echo 0 > /proc/sys/net/ipv4/ip_forward

wireguard_address: "192.168.228.1/24"
wireguard_endpoint: "84.46.100.210:55002"
wireguard_dns: 192.168.99.220, 192.168.99.230
wireguard_allowed_ips: 192.168.228.10, 192.168.223.0/24, 192.168.224.0/24, 192.168.225.0/24

#wireguard_preup:
#  - echo 1 > /proc/sys/net/ipv4/ip_forward
