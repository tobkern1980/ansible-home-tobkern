# How to use vault

```ansible-vault encrypt ssh_priv_keys.yml --vault-password-file $HOME/.vault_pass```

To fins the Valt password take  a look into the KeepassXC Database and search for *ansible vault ssh priv password*