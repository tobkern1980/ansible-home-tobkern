# ansible-home-tobkern

**Ansible Übersicht MyServer Dokumentation**

* [angular](../doc/development/angular)
* [ansible](../doc/ansible/ansible.md)
* [ansible-tools](../doc/ansible/ansible-tools.md)
* [ansible-einrichtung](../doc/ansible/ansible-einrichtung.md)
* [ansible-inventory](../doc/ansible/ansible-inventory.md)
* [ansible-vault](../doc/ansible/ansible-vault.md)
* [ansible-ssh-keys](../doc/ansible/ansible-ssh-keys.md)
* [ansible-windows](../doc/ansible/ansible-windows.md)
* [ansible-specific-distribution-version](../doc/ansible/ansible-specific-distribution-version.md)
* [just command executing easy](https://github.com/casey/just)

## Ansible eigene Variablen:

- **hostnames** = Setzen des localen honstnames für den node
- **pip_install_packages** = Liste von packeten die mit pip instaliert werden sollen
- **role_list** = Liste von rollen für das playbook
- **git_repo_list** = Liste von git Repos die eingerichtet werden sollen
- **ic2_users** = Liste von Icinga2 Usern
- **users_groups** =  Allgemeine gruppen die eingerichtet werden sollen
- **stage**
- **env**

## Ansible eigene listen

- **pip_install_packages** = pip Packete die installiert werden sollen
- **ansible_roles**
- **ansible_collctions**
- **git_repos_list**
- ****
- ****
- ****
- ****
- ****

## Ansible Tags

- **always** =  Immer ausführen. Updates aber ohne laufende Services
- **update** =  Bei updates
- **setup-all** = Bei setup für alle operativen systeme ausführen. Umfast auch Updates von services
- **install-all** = wie setup-all nur ist es eine neue installation dauert daher länger

## Ansible Tasks in der ansible-common-myserver role

- **sanity_check** = Plausibilitäts test wie Gesetzte Parapeter, Ansible Version, Deprecations,  
- **ansible-pull** = Wird auf den remote nodes eingesetzt um änderungen anzuwenden.

## Ansible Workstation

* [spacevim](https://spacevim.org/)
